<?php
	
########################################################################
# (c) 2015 Mirko Brunner
# mirko.brunner@googlemail.com
# 
# This file stay under GNU v3 Lizenz. Have fun.
########################################################################
	
class MBInfo{
    
    protected $version = "1.0.0";
    protected $state = "stable";
    
    public static function pServer()
    {
        echo '<pre>';
        var_dump($_SERVER);
        echo '</pre>';
    }

    public static function pReq()
    {
        echo '<pre>';
        var_dump($_REQUEST);
        echo '</pre>';
    }
    
    public static function pGlob()
    {
        echo '<pre>';
        var_dump($GLOBALS);
        echo '</pre>';
    }
    
    public static function pEnv()
    {
        echo '<pre>';
        var_dump($_ENV);
        echo '</pre>';
    }
    
    public static function pCook()
    {
        echo '<pre>';
        var_dump($_COOKIE);
        echo '</pre>';
    }
    
    public static function pSess()
    {
        echo '<pre>';
        var_dump($_SESSION);
        echo '</pre>';
    }
    
    public static function pInfo($group)
    {
        switch($group)
        {
            case ('general' || 'g') :
                php_info(1);
                break;
            case ('credits' || 'cr') :
                php_info(2);
                break;
            case ('conf' || 'c') :
                php_info(4);
                break;
            case ('module' || 'mod' || 'm') :
                php_info(8);
                break;
            case ('enviroment' || 'env' || 'e') :
                php_info(16);
                break;
            case ('variables' || 'val' || 'v') :
                php_info(32);
                break;
            case ('license' || 'l') :
                php_info(64);
                break;
            case ('all' || 'a') :
                php_info(-1);
                break;
            default :
                php_info(-1);
                break;
        }
    } 
}
?>